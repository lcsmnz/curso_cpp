//============================================================================
// Name        : 006_Array_vector.cpp
// Author      : Lucas Muniz
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <array>
#include <algorithm>


using namespace std;

int main()
{

	vector<int>   MyVector{11,92,55,47,74,68};
	array <int,5> MyArray{12,52,11, 66, 100};
	array <int*, 5> MyPArray;

	MyPArray[0] = &MyArray[0];
	MyPArray[1] = &MyArray[1];
	MyPArray[2] = &MyArray[2];
	MyPArray[3] = &MyArray[3];
	MyPArray[4] = &MyArray[4];


	int i,j;


	cout<<"Vector"<<endl;
	int size_vec = (int)MyVector.size();

	for(i=0;i<size_vec;i++)
	{
		cout << MyVector[i]<< endl;
	}

	cout<<"Array"<<endl;
	int size_arr = (int)MyArray.size();
	for(j=0;j<size_arr;j++)
	{
		cout << MyArray[j]<< endl;
	}

	cout<<"sorting"<<endl;

	sort(MyArray.begin(), MyArray.end());
	sort(MyVector.begin(), MyVector.end());

	cout<<"Vector"<<endl;
	for(i=0;i<size_vec;i++)
	{
		cout << MyVector[i]<< endl;
	}

	cout<<"Array"<<endl;
	for(j=0;j<size_arr;j++)
	{
		cout << MyArray[j]<< endl;
	}
	return 0;
}
