//============================================================================
// Name        : 003_Simple_Input.cpp
// Author      : Lucas Muniz
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int SquareOfSum(int A, int B);
int SquareOfDiff(int A, int B);
int CubeOfSum(int A, int B);
int CubeOfDiff(int A, int B);

int main() {

	/*
	 * (A+B)^2
	 * (A-B)^2
	 * (A+B)^3
	 * (A-B)^3
	 */

	int A{1};
	int B{1};
	int OUTPUT=0;

	while( (A!=0) && (B!=0) )
	{
		cout << "Input value for A"<< endl;
		cin >> A;

		cout << "Input value for B"<< endl;
		cin >> B;

		//(A+B)^2
		OUTPUT = SquareOfSum(A, B);
		cout << "(A+B)^2 = (" << A << "+" << B <<")^2 = " << OUTPUT << endl;

		//(A-B)^2
		OUTPUT = SquareOfDiff(A, B);
		cout << "(A-B)^2 = (" << A << "-" << B <<")^2 = " << OUTPUT << endl;

		//(A+B)^3
		OUTPUT = CubeOfSum(A, B);
		cout << "(A+B)^3 = (" << A << "+" << B <<")^3 = " << OUTPUT << endl;

		//(A-B)^3
		OUTPUT = CubeOfDiff(A, B);
		cout << "(A-B)^3 = (" << A << "-" << B <<")^3 = " << OUTPUT << endl;
	}
	return 0;
}

//(A+B)^2
int SquareOfSum(int A, int B)
{
	int OUTPUT;
	OUTPUT = ((A+B)*(A+B));
	return OUTPUT;
}

//(A-B)^2
int SquareOfDiff(int A, int B)
{
	int OUTPUT;
	OUTPUT = ((A-B)*(A-B));
	return OUTPUT;
}

//(A+B)^3
int CubeOfSum(int A, int B)
{
	int OUTPUT;
	OUTPUT = ((A+B)*(A+B)*(A+B));
	return OUTPUT;
}

//(A-B)^3
int CubeOfDiff(int A, int B)
{
	int OUTPUT;
	OUTPUT = ((A-B)*(A-B)*(A-B));
	return OUTPUT;
}

