//============================================================================
// Name        : 007_functionRetByRef.cpp
// Author      : Lucas Muniz
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int RetByValue(void)
{
	return(5);
}

int &RetByRef()
{
	static int Val;
	Val = 5;

	return(Val);
}

int main() {

	int ByValue;
	int ByRef;

	ByValue = RetByValue();
	ByRef   = RetByRef();

	return 0;
}
