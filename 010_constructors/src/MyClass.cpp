/*
 * MyClass.cpp
 *
 *  Created on: 2 de fev. de 2022
 *      Author: Lucas Muniz
 */

#include <MyClass.h>


/*
 * Move example constructors and destructors
 */
MoveExample::~MoveExample()
{
	delete pstr;
}

MoveExample::MoveExample(const string &str)
{
	pstr = new string (str);
}

MoveExample::MoveExample(const MoveExample &param)
{
	pstr = new string (param.deepCopy());
}

MoveExample::MoveExample(MoveExample &&param)
{
	pstr = param.pstr;
	param.pstr = nullptr;
}


/*
 * Rectangle constructors and destructors
 */

Rectangle::Rectangle(const Rectangle &other)
{
	Width  = other.Width;
	Height = other.Height;
}
