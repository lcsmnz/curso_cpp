//============================================================================
// Name        : 010_constructors.cpp
// Author      : Lucas Muniz
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <MyClass.h>
using namespace std;


int main()
{


	MoveExample My_Orig("this string is a test ");
	MoveExample My_Copy = MoveExample("this is Another string test ");

	cout << "string original content: " << My_Orig.deepCopy() << endl;

	cout << "string copy content: " << My_Copy.deepCopy() << endl;

	My_Orig = My_Orig + My_Copy;

	cout << "string original new content: " << My_Orig.deepCopy() << endl;

	cout << "string copy new content: " << My_Copy.deepCopy() << endl;

	Rectangle rect_1;
	Rectangle rect_2(10,20);

	cout << rect_2.CalculateArea()<<endl;

	return 0;
}
