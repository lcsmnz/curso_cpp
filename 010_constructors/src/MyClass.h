/*
 * MyClass.h
 *
 *  Created on: 2 de fev. de 2022
 *      Author: Lucas Muniz
 */

#ifndef MYCLASS_H_
#define MYCLASS_H_

#include <iostream>
#include <string>

using namespace std;

class MoveExample {
	string *pstr;

public:
	~MoveExample();
	MoveExample(const string &str);
	MoveExample(const MoveExample &param);
	MoveExample(MoveExample &&param);
	MoveExample &operator=(MoveExample &&param)
	{
		delete pstr;
		pstr = param.pstr;
		param.pstr = nullptr;
		return *this;
	}

	const string &ReadContent() const	{ return *pstr;	}

	const string &deepCopy() const	{ return *pstr;	}

	MoveExample operator+(const MoveExample &RHS)
	{
		return MoveExample(ReadContent()+RHS.ReadContent());
	}


private:
};

class Rectangle {

	int Width, Height;

public:
	Rectangle(int x, int y) : Width(x), Height(y) {} ;
	Rectangle() = default;
	Rectangle(const Rectangle &other);


	int CalculateArea() {return Width * Height;}


private:
};


#endif /* MYCLASS_H_ */

