/*
 * my_types.h
 *
 *  Created on: 25 de jan. de 2022
 *      Author: Lucas Muniz
 */

#ifndef MY_TYPES_H_
#define MY_TYPES_H_

/*
 * ENUM
 */
enum Months_e
{
	January=1,
	February,
	March,
	April,
	May,
	June,
	July,
	August,
	September,
	October,
	November,
	December,
};

/*
 * ENUM CLASS
 */
enum class DayOfWeek_e
{
	Monday,
	Tuesday,
	Wednesday,
	Thursday,
	Friday,
	Saturday,
	Sunday,
};


/*
 * Struct
 */

typedef struct Teams_t
{
	std::string TeamName;
	int Players;
	int MatchesPlayed;
	int Position;
};
#endif /* MY_TYPES_H_ */
