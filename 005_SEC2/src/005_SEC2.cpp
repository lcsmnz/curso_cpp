//============================================================================
// Name        : 005_SEC2.cpp
// Author      : Lucas Muniz
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "my_types.h"

using namespace std;

int main()
{
	Months_e    ThisMonth    = January;
	Months_e    MyBirthMonth = September;
    DayOfWeek_e Today        = DayOfWeek_e::Saturday;

    Teams_t MyTeam;

    MyTeam.TeamName = "Bahia";
    MyTeam.Players = 11;
    MyTeam.Position = 15;
    MyTeam.MatchesPlayed = 10;


	cout << "January is the "<< ThisMonth <<"st month" << endl;
	cout << "My birthday is in the "<< MyBirthMonth<<"th month"<< endl;

	if((Today == DayOfWeek_e::Saturday)||(Today == DayOfWeek_e::Sunday))
	{
		cout << "No need to work today"<< endl;
		cout << "Watch a game of " << MyTeam.TeamName << endl;
	}
	else
	{
		cout << "You should work today" << endl;
	}



	return 0;
}

