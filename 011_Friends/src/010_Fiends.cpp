//============================================================================
// Name        : 010_Fiends.cpp
// Author      : Lucas Muniz
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

class Square;

class Rectangle {

	int Width, Height;

public:
	Rectangle(){}
	Rectangle(int x, int y) : Width(x), Height(y) {} ;

	int CalculateArea() {return Width * Height;}

	friend Rectangle Duplicate(const Rectangle &param);
	void Convert(Square Sqr);

private:
};



Rectangle Duplicate(const Rectangle &param)
{
	Rectangle TempRect;

	TempRect.Width  = 2 * param.Width;
	TempRect.Height = 2 * param.Height;

	return TempRect;
}


class Square {

	friend class Rectangle;

private:
	int Side;

public:
	Square(int a): Side(a) {}

};

void Rectangle::Convert(Square Sqr)
{
	Width = Sqr.Side;
	Height = Sqr.Side;
}


int main()
{
	Rectangle Rect1;
	Rectangle Rect2(2,3);

	Square Sqr1(5);

	Rect1 = Duplicate(Rect2);

	cout << Rect1.CalculateArea()<<"\n"<< Rect2.CalculateArea() << '\n';

	Rect1.Convert(Sqr1);

	cout << Rect1.CalculateArea()<<endl;

	return 0;
}
