//============================================================================
// Name        : 008_Class_into.cpp
// Author      : Lucas Muniz
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

class Rectangle
{
	int width, heigth;
public:
	Rectangle(void);
	Rectangle(int, int);
	int Calculate_Area(void);
	void DoNothing(int x);

private:

};

Rectangle::Rectangle(void)
{
	width = 10;
	heigth = 10;
}

Rectangle::Rectangle(int W, int H)
{
	width = W;
	heigth = H;
}

void Rectangle::DoNothing(int x)
{
	cout << x << endl;
}

int Rectangle::Calculate_Area(void)
{
	return (width * heigth);
}


class Circle
{
	double radius;
public:
	Circle(double r) : radius(r) {}

	double Calculate_Area()
	{
		return (radius * radius * 3.14);
	}

private:
};


class Cylinder
{
	Circle Base;
	double Heigth;

public:
	Cylinder(double r, double H) : Base(r), Heigth(H) {}
	double Calculate_Volume()
	{
		return (Base.Calculate_Area() * Heigth );
	}
private:
};

int main()
{
	Rectangle Rect_1;
	Rectangle Rect_2(7,7);
	Rectangle Rect_3{8,8};

	Rectangle *pRect_1, *pRect_2, *pRect_3;

	Circle Circ_1(2);

	Cylinder Cylin(2,2);


	pRect_1 = &Rect_1;
	pRect_2 = new Rectangle(7,7);
	pRect_3 = new Rectangle[2]{ {6, 3}, {12, 6} };

	cout << " rec 1 area: " << Rect_1.Calculate_Area() <<endl;
	cout << " *prec_1 area: " << pRect_1->Calculate_Area() <<endl;

	cout << " rec 2 area: " << Rect_2.Calculate_Area() <<endl;
	cout << " *prec_2 area: " << pRect_2->Calculate_Area() <<endl;


	cout << " rec 3 area: " << Rect_3.Calculate_Area() <<endl;
	cout << " *prec_3[0] area: " << pRect_3[0].Calculate_Area() <<endl;
	cout << " *prec_3[1] area: " << pRect_3[1].Calculate_Area() <<endl;

	cout << Circ_1.Calculate_Area() <<endl;
	cout << Cylin.Calculate_Volume() <<endl;

	return 0;
}
