//============================================================================
// Name        : 012_inheritance.cpp
// Author      : Lucas Muniz
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;


class Output {
public:

	static void PrintToScreen(int Content);

};

void Output::PrintToScreen(int Content)
{
	cout << Content << endl;
}

class Polygon
{

protected:
	int Width;
	int Height;

public:
	Polygon(int a, int b) : Width(a), Height(b) {}
	void InitPolygon(int a, int b)
	{
		Width = a;
		Height = b;
	}
};

class Rectangle : public Polygon, public Output
{
public:

	Rectangle(int a, int b) : Polygon(a,b){}

	int CalculateArea()
	{
		return Width*Height;
	}
};

class Triangle : public Polygon, public Output
{
public:

	Triangle(int a, int b) : Polygon(a,b){}
	int CalculateArea()
	{
		return (Width*Height)/2;
	}
};

class Mother {
public:
	Mother()
	{
		cout << "Mother class created, no param \n";
	}
	Mother(int a)
	{
		cout << "Mother class created, with param \n";
	}
private:
};

class Daughter : public Mother
{
public:
	Daughter(int a)
	{
		cout << "Daughter class created, with param \n";
	}
};

class Son : public Mother
{
public:
	Son(int a) : Mother(a)
	{
		cout << "Son class created, with param \n";
	}
};

int main() {

	Rectangle Rect1(10,20);
	Triangle Triangle1(10,20);



	cout << 	Rect1.CalculateArea() << endl;
	cout << Triangle1.CalculateArea() << endl;

	Rect1.PrintToScreen(Rect1.CalculateArea());
	Triangle1.PrintToScreen(Triangle1.CalculateArea());


	Daughter Lia(0);
	/*
	 *  Mother class created, no param
	 *	Daugter class created, with param
	 */

	Son Pedro(0);




	return 0;
}
