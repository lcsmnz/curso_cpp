//============================================================================
// Name        : 009_class_p2.cpp
// Author      : Lucas Muniz
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

class Vector2D
{
public:
	int x,y;
	static int Vectors;
	Vector2D() {Vectors++;};
	Vector2D(int a, int b){ x=a;y=b;Vectors++;}
	//Vector2D operator+(const Vector2D&);
private:


};

int Vector2D::Vectors = 0;

Vector2D operator+(const Vector2D& LHS, const Vector2D& RHS)
{
	Vector2D TempVector;

	TempVector.x = LHS.x + RHS.x;
	TempVector.y = LHS.y + RHS.y;

	return TempVector;
}

template <class T>
class ValuePairs
{
	T a,b;

public:
	ValuePairs(T first, T second)
	{
		a = first;
		b = second;
	}

	T GetMax();
};

template <class T>
T ValuePairs<T>::GetMax()
{
	T result;
	if(a>b){result = a;}
	else{result =  b;}

	return result;
}

int main()
{
	Vector2D Vec_1(3,1);
	Vector2D Vec_2(1,2);
	Vector2D ResVec;

	ValuePairs<int> intType(10,25);
	ValuePairs<float> floatType(10.1,25.9);

	ResVec = Vec_1 + Vec_2;

	cout << intType.GetMax() <<" hello " << floatType.GetMax() << endl;

	return 0;
}
