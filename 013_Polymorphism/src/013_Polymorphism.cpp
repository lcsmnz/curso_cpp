//============================================================================
// Name        : 013_Polymorphism.cpp
// Author      : Lucas Muniz
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

class Polygon
{
protected:
	int Width;
	int Height;

public:
	Polygon(int a, int b)
	{
		 Width = a;
		 Height = b;
	}
	virtual int CalculateArea()
	{
		return 0;
	}
};

class Rectangle : public Polygon
{
public:

	Rectangle(int a, int b) : Polygon(a,b) {}
	int CalculateArea()
	{
		return Width*Height;
	}
};

class Triangle : public Polygon
{
public:
	Triangle(int a, int b) : Polygon(a,b) {}
	int CalculateArea()
	{
		return (Width*Height)/2;
	}
};

int main()
{

	Polygon *pPolyRect = new Rectangle(4,5);
	Polygon *pPolyTri  = new Triangle(4,5);
	Polygon *pPoly     = new Polygon(4,5);

	cout<<pPolyRect->CalculateArea()<<endl;
	cout<<pPolyTri->CalculateArea()<<endl;
	cout<<pPoly->CalculateArea()<<endl;

	delete pPolyRect;
	delete pPolyTri;
	delete pPoly;

	return 0;
}
